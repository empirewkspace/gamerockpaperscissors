import { Camera, CameraOptions } from '@ionic-native/camera';
import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable()
export class DeviceProvider {

  private options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,    
    correctOrientation:false,
    allowEdit:false,
    saveToPhotoAlbum:false,
    cameraDirection:this.camera.Direction.BACK,
    sourceType:this.camera.PictureSourceType.CAMERA,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  }

  constructor(private camera: Camera,
    private sanitizer: DomSanitizer
  ) {
    
  }

  TakePicture():Promise<any> {
    return this.camera.getPicture(this.options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData; 
      return base64Image;      
    }, (err) => {
      console.log(err);
    });

  }



}
