import { Injectable } from '@angular/core';
import { ModalController } from 'ionic-angular';

export interface Scene {
  scene: number;
  image: string | string[];
  subtitles: string[];
  audio: string[];
  musicOnEnter?: string;
  musicOnLeave?: string;
  loop?: boolean;
}

export interface Movie {
  movie: number;
  theme: string;
  scenes: number[];
  musicOnEnter?: string;
  musicOnLeave?: string;
  loop?: boolean;
}

@Injectable()
export class SceneProvider {

  public sceneMatrix: Scene[] = [
    { scene: 45, image: 'emperorWelcome0.png', subtitles: ["Welcome my apprentice", "I have foreseen your presence here"], audio: ['emperorWelcomeA', 'emperorWelcomeB']},
    { scene: 46, image: 'emperorWelcome1.jpg', subtitles: ["I feel great rage on you...", "They shouldn't have taken your Teddy Bear away"], audio: ['emperorWelcomeC', 'emperorWelcomeD']},
    { scene: 47, image: 'emperorWelcome2.jpeg',subtitles: ["Now.. you are heeere", "ha ha ha ha ha ha ha"], audio: ['emperorWelcomeE', 'emperorPleased4']},
    { scene: 48, image: 'emperorWelcome3.jpg', subtitles: ["I have selected Lord Vader to be your teacher...", "GO to the conference room and wait for him"], audio: ['emperorWelcomeF', 'emperorWelcomeG']},
    { scene: 49, image: 'emperorWelcome4.jpg', subtitles: ["I told you not to eat my burritos!!!", "I find your lack of attention distuuurbing", "Now... you'll pay for your insolence"], audio: ['emperorWelcomeH', 'emperorWelcomeI', 'emperorWelcomeJ']},
    { scene: 50, image: 'emperorWelcome5.jpg', subtitles: ["Lord Vader, huh.. excuse-me sir?", "WHAT IS IT? Don't interrupt me when I'm...", "Another student sir! The emperor assigned you to be his teacher", "Probably another looser..."], audio: ['emperorWelcomeK', 'emperorWelcomeL', 'emperorWelcomeM', 'emperorWelcomeN']},
    { scene: 51, image: 'emperorWelcome6.jpg', subtitles: ["You have asked for me? My master...", "I have selected a new apprentice for you.. he is already in the Conference room awaiting for your instructions!", "Don't fail me on this one VADER, or you will be punished!", "..BUT the last one... HE WAS A WOOSS.. I can't be blamed for that?!?", "Enough! I'll not tolerate insubordination. Don't fail me again Vader. You were warned.","Yes My Master"], audio: ['emperorWelcomeP', 'emperorWelcomeQ', 'emperorWelcomeR', 'emperorWelcomeS', 'emperorWelcomeT', 'emperorWelcomeU']},
    { scene: 52, image: 'emperorWelcome7.jpg', subtitles: ["Hello loss..A..Apprentice! The emperor have instructed me to teach you on the arts of the Sith", "Every fight lasts 20 rounds", "I'll be monitoring your skills with the force PERSONALLY", "Try to focus your rage and feel your opponents move", "Now, leave and create a fight! I'll be watching"], audio: ['emperorWelcomeV', 'emperorWelcomeW', 'emperorWelcomeX', 'emperorWelcomeY', 'emperorWelcomeZ']},
    { scene: 1, image: 'vaderDisbelief1.jpg', subtitles: ['Why the force brought this looser to me!'], audio: ['vaderDisbelief1'] },
    { scene: 2, image: 'vaderDisbelief2.jpg', subtitles: ['Perhaps I should find another way to teach him!'], audio: ['vaderDisbelief2'] },
    { scene: 3, image: 'vaderDisbelief3.jpg', subtitles: ["I don't care what you do anymore"], audio: ['vaderDisbelief3'] },
    { scene: 4, image: 'vaderDisbelief4.jpg', subtitles: ['As you are such a looser, come here and clean my b*tt!'], audio: ['vaderDisbelief4'] },
    { scene: 5, image: 'vaderDisbelief5.jpg', subtitles: ['Here.. walk my dog, because you are no good for anything else!'], audio: ['vaderDisbelief5'] },
    { scene: 6, image: 'vaderDisbelief6.jpg', subtitles: ['Oooh you are a cat person!? How SOFT for a Sith!'], audio: ['vaderDisbelief6'] },
    { scene: 7, image: 'vaderDisbelief7.jpg', subtitles: ['LOOK! Even this cat thinks you are a looser!'], audio: ['vaderDisbelief7'] },
    { scene: 8, image: 'emperorPleased1.png', subtitles: ['Your challenger is weeeeak!'], audio: ['emperorPleased1'] },
    { scene: 9, image: 'emperorPleased2.png', subtitles: ['He fled like a coward'], audio: ['emperorPleased2'] },
    {
      scene: 10, image: 'emperorPleased3.jpeg', subtitles: ['You have won this battle my apprentice', 'ha ha ha ha ha...'], audio: ['emperorPleased3',
        'emperorPleased4']
    },
    { scene: 11, image: 'emperorDispleased1.jpg', subtitles: ['You are not a Sith, you are a coward'], audio: ['emperorDispleased1'] },
    { scene: 12, image: 'emperorDispleased2.jpg', subtitles: ['Lord Vader, send this looser to a JEDI school'], audio: ['emperorDispleased2'] },
    { scene: 13, image: 'vaderFailsForTheLastTime1.jpg', subtitles: ['Gosh, I have failed! What the Emperor will do to me?'], audio: ['vaderFails1'] },
    { scene: 14, image: 'vaderFailsForTheLastTime2.jpg', subtitles: ['tan tan taran tan taran, THIS is NOT so BAAAD!', 'Lord Vader? Why am I doing this?', 'Shut up and do it fast, before anyone takes a picture of us here'], audio: ['vaderFails2', 'vaderFails3', 'vaderFails4'] },
    { scene: 15, image: 'vaderFailsForTheLastTime3.jpg', subtitles: ["That's an honor to meet you Lord Vader! Could you sign my helmet please?", "Don't say a word or I will mop your face on this floor!"], audio: ['vaderFails5', 'vaderFails6'] },
    { scene: 17, image: 'vaderVeryDispleased1.jpg', subtitles: [""], audio: ['vaderVeryDispleased1'] },
    { scene: 18, image: 'vaderVeryDispleased2.jpg', subtitles: ["Don't fail me again"], audio: ['vaderVeryDispleased2'] },
    { scene: 19, image: 'vaderVeryDispleased3.jpg', subtitles: ["How can he be such a looser?"], audio: ['vaderVeryDispleased3'] },
    { scene: 20, image: 'vaderVeryDispleased4.jpg', subtitles: ["You are a worthless bag of potatoes"], audio: ['vaderVeryDispleased4'] },
    { scene: 21, image: 'vaderVeryDispleased5.jpg', subtitles: ["You will pay for such incompetence"], audio: ['vaderVeryDispleased5'] },
    { scene: 22, image: 'vaderVeryDispleased6.jpg', subtitles: ["You Ignorant FOOL!"], audio: ['youIgnorantFool'] },
    { scene: 23, image: 'vaderDispleased1.jpg', subtitles: ["you have lost?"], audio: ['vaderDispleased1'] },
    { scene: 24, image: 'vaderDispleased2.jpg', subtitles: ["Don't loose again"], audio: ['vaderDispleased2'] },
    { scene: 25, image: 'vaderDispleased3.jpg', subtitles: ["Are you really a Sith or are you dead from the neck up?"], audio: ['vaderDispleased3'] },
    { scene: 26, image: 'vaderDispleased4.jpeg', subtitles: ["Don't make me angry"], audio: ['vaderDispleased4'] },
    { scene: 27, image: 'vaderDispleased5.jpg', subtitles: ["Tell me , does it hurt to be you?"], audio: ['vaderDispleased5'] },
    { scene: 28, image: 'vaderDispleased6.jpg', subtitles: ["I think I will have your IQ tested"], audio: ['vaderDispleased6'] },
    { scene: 29, image: 'vaderDispleased7.jpg', subtitles: ["You are a waste of our time"], audio: ['vaderDispleased7'] },
    { scene: 30, image: 'vaderDispleased8.jpg', subtitles: ["Give me your mobile and leave the room imediatelly"], audio: ['vaderDispleased8'] },
    { scene: 31, image: 'vaderDispleased9.jpg', subtitles: ["I find your lack of abilities disturbing"], audio: ['vaderDispleased9'] },
    { scene: 32, image: 'vaderVeryHappy1.jpg', subtitles: ["I am the best Sith teacher aint I?"], audio: ['vaderVeryHappy1'] },
    { scene: 33, image: 'vaderVeryHappy2.jpg', subtitles: ["You make me proud"], audio: ['vaderVeryHappy2'] },
    { scene: 34, image: 'vaderVeryHappy3.jpg', subtitles: [""], audio: ['themeVaderGuitarStarWars'] },
    { scene: 35, image: 'vaderVeryHappy4.jpg', subtitles: [""], audio: ['themeVaderGuitarImperialMarch'] },
    { scene: 36, image: 'vaderHappy1.jpg', subtitles: ["Good"], audio: ['vaderHappy1'] },
    { scene: 37, image: 'vaderHappy2.jpg', subtitles: ["Yeees, feel the rage"], audio: ['vaderHappy2'] },
    { scene: 38, image: 'vaderHappy3.jpg', subtitles: ["Excelent"], audio: ['vaderHappy3'] },
    { scene: 39, image: 'vaderHappy4.jpg', subtitles: ["Nicely done"], audio: ['vaderHappy4'] },
    { scene: 40, image: 'vaderHappy5.jpg', subtitles: ["Feel the Rage Growing more!"], audio: ['vaderHappy5'] },
    { scene: 41, image: 'vaderHappy6.jpg', subtitles: ["Yes, strike him again"], audio: ['vaderHappy6'] },
    { scene: 42, image: 'vaderHappy7.jpeg', subtitles: ["oh you are good"], audio: ['vaderHappy7'] },
    { scene: 43, image: 'vaderHappy8.jpg', subtitles: ["I can feel your powers growing"], audio: ['vaderHappy8'] }
    
  ];

  public movieMatrix: Movie[] = [
    { movie: 1, theme: 'Emperor Welcomes Apprentice', scenes: [45, 46, 47, 48, 49, 50, 51, 52],  musicOnEnter: 'themeEmperorTalking', loop: true, musicOnLeave: 'themeVaderLeaves'},
    { movie: 2, theme: 'Vader Lost - ending 1', scenes: [13, 14] },
    { movie: 3, theme: 'Vader Lost - ending 2', scenes: [13, 15] },
    { movie: 4, theme: 'Opponent fled', scenes: [8, 9, 10],  musicOnEnter: 'themeEmperorTalking', loop: true },
    { movie: 5, theme: 'You fled', scenes: [11, 12],   musicOnEnter: 'themeEmperorTalking', loop: true},
    { movie: 6, theme: 'You Won', scenes: [10, 33],   musicOnEnter: 'themeEmperorTalking', loop: true}
  ];



  constructor(public modalCtrl: ModalController) {

  }

  FetchScene(id: number): Scene {
    let scene = this.sceneMatrix.find(p => p.scene == id);
    if (scene) return scene;    
  }

  FetchMovie(id: number): Movie {
    let movie = this.movieMatrix.find(p => p.movie == id);
    if (movie) return movie;    
  }

  
}



