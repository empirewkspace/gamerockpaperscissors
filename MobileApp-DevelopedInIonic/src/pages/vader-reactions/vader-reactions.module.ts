import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VaderReactionsPage } from './vader-reactions';

@NgModule({
  declarations: [
    VaderReactionsPage,
  ],
  imports: [
    IonicPageModule.forChild(VaderReactionsPage),
  ],
})
export class VaderReactionsPageModule {}
