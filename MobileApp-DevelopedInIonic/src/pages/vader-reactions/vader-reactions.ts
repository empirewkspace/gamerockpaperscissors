import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, ViewController } from 'ionic-angular';
import { SoundMatrixProvider } from '../../providers/sound-matrix/sound-matrix';
import { Scene, Movie, SceneProvider } from '../../providers/scene/scene';

@IonicPage()
@Component({
  selector: 'page-vader-reactions',
  templateUrl: 'vader-reactions.html',
})
export class VaderReactionsPage {

  @ViewChild(Slides) slides: Slides;
  private scene: Scene;
  private movie: Movie;
  private scenes: Scene[];
  private loaded = false;
  private startAddr = 'assets/imgs/';
  private sceneImage: string;
  private subtitles: string[] = [];
  private multipleScenes = false;
  private musicIsPlaying = false;
  private title: string;
  private requestLeaveMovie: boolean = false;

  constructor(
    public navCtrl: NavController, public navParams: NavParams,
    public sound: SoundMatrixProvider,
    public view: ViewController,
    public sceneProvider: SceneProvider
  ) {
  }

  ionViewWillLeave(){
    if(this.scene) this.LeaveScene();
    if(this.movie) this.LeaveMovie();
  }

  async ionViewDidEnter() {
    this.scene = this.navParams.get('Scene');
    this.movie = this.navParams.get('Movie');
    this.title = this.navParams.get('Title');
        
    if (this.scene) {      
      try {
        await this.PlayScene(this.scene);
        await this.LeaveScene();
      } catch (e) { console.log(e) }
    }

    if (this.movie) {
      try {
        this.title=this.movie.theme;        
        await this.PlayMovie();
        await this.LeaveMovie();
      } catch (e) { console.log(e)}
    }

    this.view.dismiss();
  }

  Close(){
    this.requestLeaveMovie = true;
    this.view.dismiss();
  }

  async PlayScene(scene): Promise<boolean> {
    this.subtitles=[];
    if (scene.musicOnEnter) this.LoopSceneMusic();    
    this.sceneImage = this.startAddr + scene.image;
    this.loaded = true;
    let index = 0;
    while (index < scene.subtitles.length) {
      this.subtitles.push(scene.subtitles[index]);
      try {
        await this.sound.Play(scene.audio[index])
      } catch (e) { console.log(e) }
      if(this.requestLeaveMovie) break;
      index++;
    }        
    return Promise.resolve(true);
  }

  async PlayMovie():Promise<boolean> {
    this.scenes = [];
    this.movie.scenes.forEach(p =>this.scenes.push(this.sceneProvider.FetchScene(p)));
    if (this.movie.musicOnEnter) this.LoopMovieMusic();
    let i = 0;
    while (i < this.scenes.length) {
      await this.PlayScene(this.scenes[i]);
      i++;
      if(this.requestLeaveMovie) break;
    }
    return Promise.resolve(true);    
  }


  LoopSceneMusic() {
    this.musicIsPlaying=true;
    this.sound.PlayMusicOnLoop(this.scene.musicOnEnter);
  }

  LoopMovieMusic() {
    this.musicIsPlaying=true;
   this.sound.PlayMusicOnLoop(this.movie.musicOnEnter);
  }

  async LeaveScene(): Promise<boolean> {
    this.scene.loop = false;
    if (this.musicIsPlaying) await this.sound.Stop(this.scene.musicOnEnter)
    if (this.scene.musicOnLeave) {
      await this.sound.Play(this.scene.musicOnLeave);
    }
    return Promise.resolve(true);
  }

  async LeaveMovie(): Promise<boolean> {
    this.movie.loop = false;
    if (this.musicIsPlaying) await this.sound.Stop(this.movie.musicOnEnter)
    if (this.movie.musicOnLeave) {
      await this.sound.Play(this.movie.musicOnLeave);
    }
    return Promise.resolve(true);
  }




}
