import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChoiceUserPage } from './choice-user';

@NgModule({
  declarations: [
    ChoiceUserPage,
  ],
  imports: [
    IonicPageModule.forChild(ChoiceUserPage),
  ],
})
export class ChoiceUserPageModule {}
