import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { game, ApiProvider } from '../../providers/api/api';
import { SoundMatrixProvider } from '../../providers/sound-matrix/sound-matrix';
import { GameLogicProvider } from '../../providers/game-logic/game-logic';

@IonicPage()
@Component({
  selector: 'page-game',
  templateUrl: 'game.html',
})
export class GamePage {

  private userName: string;
  private possibilities=['paper','scissors','rock'];
  private gameResult: game;
  private objs=[];
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public api: ApiProvider,
    public sound: SoundMatrixProvider,
    public modalCtrl: ModalController,
    public gameApi: GameLogicProvider
  ) {
  }

  ionViewWillEnter(){    
    this.userName=this.navParams.get('userName'); 
    let i=0;
    while (i<3) {this.objs.push(this.gameApi.objects[i]); i++;}           
  } 

  
  Play(){    
    let myModal = this.modalCtrl.create('ChoiceUserPage', { 'options': this.objs });
    myModal.present();
    myModal.onDidDismiss((userChoice)=>{      
      this.api.callServer({result:userChoice,name:this.userName}).then((data:game)=>{
        this.gameResult = data;
      });
    });           
  }

}
