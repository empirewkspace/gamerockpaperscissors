import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GameVaderPage } from './game-vader';

@NgModule({
  declarations: [
    GameVaderPage,
  ],
  imports: [
    IonicPageModule.forChild(GameVaderPage),
  ],
})
export class GameVaderPageModule {}
