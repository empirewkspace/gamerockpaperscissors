**Game Rock Paper Scissors Spock Lizzards - Star Wars parody - Version 1.5.0**
Author: Camilo Chaves  
Objective: Developed in 2 weeks to demonstrate coding capacity on Node.js+Express.js+Mongoose+MongoDB Cloud+Ionic Framework  

##Google Play Version 1.5.0

Link to [download game on Google Play](https://play.google.com/store/apps/details?id=com.rockpaperscissors.camilo)  

##Apple Store Version 1.4.2

Link to [download game on Apple Store](https://itunes.apple.com/us/app/funny-rock-paper-scissors/id1431974865?l=pt&ls=1&mt=8)

---