module.exports = function (handler) {
    return async (req, res, next) => {
        try {
            //...
            await handler(req, res);
        } catch (ex) {
            //vai chamar o error middleware que foi definido 
            //por último no index.js
            next(ex);
        }
    };
}
