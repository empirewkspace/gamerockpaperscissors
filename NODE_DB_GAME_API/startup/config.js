const config = require('config');

module.exports = function () {
    //config module
    if (!config.get('jwtPrivateKey')) {
        throw new Error('FATAL ERROR: jwtPrivateKey is not defined');        
    }
}