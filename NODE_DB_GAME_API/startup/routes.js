const express = require('express');
const error = require('../middlewares/error');
const users = require('../routes/users');
const auth = require('../routes/auth');
const game = require('../routes/game');

module.exports = function (app) {
    //Middlewares
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Origin, x-auth-token");
        res.header('Access-Control-Allow-Methods', '*'); 
        next();
    });
    app.use(express.json());    
    app.use(express.urlencoded({extended: true}));
    app.use(express.static('public'));
    
    //Rotas
    app.use('/api/users', users);
    app.use('/api/auth', auth);
    app.use('/api/game', game);

    //error middleware after all middlewares and routes
    app.use(error);
}